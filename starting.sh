#
# after cloning dotfiles repo.
# git clone https://bitbucket.org/fktan5/dotfiles.git .dotfiles
#
#!/bin/bash

if [ ! -e ~/.oh-my-zsh ]; then
  curl -L http://install.ohmyz.sh | sh
fi

git clone https://github.com/rbenv/rbenv.git ~/.rbenv

git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build

curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > installer.sh
sh ./installer.sh ~/.vim/

sudo cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

if [ ! -e ~/.zshrc ]; then
  ln -s .dotfiles/zshrc .zshrc
fi

if [ ! -e ~/.vimrc ]; then
  ln -s .dotfiles/vimrc .vimrc
fi

if [ ! -e ~/.gitconfig ]; then
  ln -s .dotfiles/gitconfig .gitconfig
fi

if [ ! -e ~/.tmux.conf ]; then
  ln -s .dotfiles/tmux.conf .tmux.conf
fi
