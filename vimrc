"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=$HOME/.vim/repos/github.com/Shougo/dein.vim

" Required:
call dein#begin('$HOME/.vim')

" Let dein manage dein
" Required:
call dein#add('Shougo/dein.vim')

" Add or remove your plugins here:
call dein#add('Shougo/neosnippet.vim')
call dein#add('Shougo/neosnippet-snippets')

" You can specify revision/branch/tag.
call dein#add('Shougo/vimshell', { 'rev': '3787e5' })

call dein#add('Shougo/neocomplete.vim')
call dein#add('Shougo/unite.vim')
call dein#add('scrooloose/nerdtree') "sidebar tree
call dein#add('w0ng/vim-hybrid') "color
call dein#add('tpope/vim-commentary') "commentout
call dein#add('tpope/vim-fugitive')   "commentout
call dein#add('kannokanno/previm')     "markdown
call dein#add('tyru/open-browser.vim') "markdown

call dein#add('tpope/vim-surround')
call dein#add('mattn/emmet-vim')
call dein#add('hail2u/vim-css3-syntax')
call dein#add('jelera/vim-javascript-syntax')
call dein#add('mattn/jscomplete-vim')
setl omnifunc=jscomplete#CompleteJS
call dein#add('scrooloose/syntastic') "syntax check

" Required:
call dein#end()

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif

"End dein Scripts-------------------------

" Required:
filetype plugin indent on

" neocomplete "
let g:acp_enableAtStartup = 0
let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_smart_case = 1

""""" vimOption 
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8

set tabstop=2
set softtabstop=0
set shiftwidth=2
set expandtab
set backspace=indent,eol,start " *
set smartindent

if v:version >= 800
  set termguicolors
elseif v:version > 741
  set guicolors
endif

" color / visual
syntax on " *
set ruler
set number
set list
set listchars=tab:>.,trail:_,eol:↲,extends:>,precedes:<,nbsp:%
set sidescrolloff=4
set showmatch
set matchpairs& matchpairs+=<:>
set gcr=a:blinkon0
set scrolloff=4
set laststatus=2
set background=dark
set term=xterm-256color
set t_Co=256
set hlsearch
set incsearch
set background=dark
autocmd ColorScheme * highlight LineNr ctermfg=247 guifg=NONE

"Color Scheme
colorscheme hybrid

if !exists('g:not_finsh_neobundle')
endif

hi Pmenu      ctermfg=254 ctermbg=25
hi PmenuSel   ctermfg=232 ctermbg=86
hi PmenuSbar  ctermfg=0 ctermbg=250
hi PmenuThumb ctermfg=0 ctermbg=250

" mouse
set mouse=a
set ttymouse=xterm2
set mousemodel=popup

" Abbreviations
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall

" Mapping
"" Split
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>

"" Git
noremap <Leader>ga :Gwrite<CR>
noremap <Leader>gc :Gcommit<CR>
noremap <Leader>gsh :Gpush<CR>
noremap <Leader>gll :Gpull<CR>
noremap <Leader>gs :Gstatus<CR>
noremap <Leader>gb :Gblame<CR>
noremap <Leader>gd :Gvdiff<CR>
noremap <Leader>gr :Gremove<CR>

" session management
nnoremap <leader>so :OpenSession
nnoremap <leader>ss :SaveSession
nnoremap <leader>sd :DeleteSession<CR>
nnoremap <leader>sc :CloseSession<CR>

"  Tabs
nnoremap <Tab> gt
nnoremap <S-Tab> gT
nnoremap <silent> <S-t> :tabnew<CR>

" NERDTree configuration
let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['\.rbc$', '\~$', '\.pyc$', '\.db$', '\.sqlite$', '__pycache__']
let g:NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_focus_on_files=1
let g:NERDTreeMapOpenInTabSilent = '<RightMouse>'
let g:NERDTreeWinSize = 32
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite
nnoremap <silent> <F2> :NERDTreeFind<CR>
noremap <F3> :NERDTreeToggle<CR>

" Syntastic configuration
let g:syntastic_mode_map = { 'mode': 'passive',
            \ 'active_filetypes': ['ruby', 'javascript','coffee', 'scss'] }
let g:syntastic_ruby_checkers = ['rubocop'] " or ['rubocop', 'mri']
let g:syntastic_javascript_checkers = ['jshint']
let g:syntastic_coffee_checkers = ['coffeelint']
let g:syntastic_scss_checkers = ['scss_lint']
let g:syntastic_error_symbol='✗'
let g:syntastic_warning_symbol='⚠'
let g:syntastic_style_error_symbol = '✗'
let g:syntastic_style_warning_symbol = '⚠'
let g:syntastic_check_on_wq = 0
hi SyntasticErrorSign ctermfg=160
hi SyntasticWarningSign ctermfg=220

set runtimepath+=${HOME}/.dotfiles/
runtime! vim.conf/*.vim
