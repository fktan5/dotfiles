#-#omz
export ZSH=$HOME/.oh-my-zsh
source $ZSH/oh-my-zsh.sh
plugins=(git ruby gem rails vagrant svn)

#zmv
autoload zmv
alias zmv='noglob zmv'

#use color
autoload -Uz colors
colors

#completion
autoload -Uz compinit
compinit

#prompt color 
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git svn
zstyle ':vcs_info:*' max-exports 6 # formatに入る変数の最大数
zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git:*' formats '%b@%r' '%c' '%u'
zstyle ':vcs_info:git:*' actionformats '%b@%r|%a' '%c' '%u'
setopt prompt_subst
function vcs_echo {
    local st branch color
    STY= LANG=en_US.UTF-8 vcs_info
    st=`git status 2> /dev/null`
    if [[ -z "$st" ]]; then return; fi
    branch="$vcs_info_msg_0_"
    if   [[ -n "$vcs_info_msg_1_" ]]; then color=${fg[green]} #staged
    elif [[ -n "$vcs_info_msg_2_" ]]; then color=${fg[red]} #unstaged
    elif [[ -n `echo "$st" | grep "^Untracked"` ]]; then color=${fg[blue]} # untracked
    else color=${fg[cyan]}
    fi
    echo "%{$color%}(%{$branch%})%{$reset_color%}" | sed -e s/@/"%F{yellow}@%f%{$color%}"/
}
PROMPT='%K{7}%F{1}◆ %K{reset_color} %F{3}%n%F{7}@%F{5}%M %F{4}%~
%F{2}%*%F{7} `vcs_echo`%(?.$.%F{red}$%f)%F{reset_color}%K{reset_color} '
PROMPT2='>>'

#zsh history
setopt pushd_ignore_dups
HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=1000000

#zsh history peco function
# expected : /usr/local/bin/peco
function peco-select-history() {
    local tac
    if which tac > /dev/null; then
        tac="tac"
    else
        tac="tail -r"
    fi
    BUFFER=$(\history -n 1 | \
        eval $tac | \
        peco --query "$LBUFFER")
    CURSOR=$#BUFFER
    zle clear-screen
}

if [ -e /usr/local/bin/peco ]; then 
  zle -N peco-select-history
  bindkey '^r' peco-select-history
fi

#alias
alias ll='ls -alh'
alias la='ls -a'
alias lll='ls -l'
alias grepcnf='grep -v -e "#" -e "^$"'
alias vi='vim'
alias ss='ls -a'
alias view='vim -R'
alias tree='tree -C'
alias today="date '+%Y%m%d'"

if type colordiff > /dev/null 2>&1; then
  alias diff="colordiff"
fi

alias bx='bundle exec'
alias bxrspc='bundle exec rspec'

case ${OSTYPE} in
  darwin*)
    export CLTCOLOR=1
    alias ls='ls -G -F'
  ;;
  linux*)
  ;;
esac

DISABLE_UPDATE_PROMPT=true

#zsh_notify
export SYS_NOTIFIER="$HOME/.rbenv/shims/terminal-notifier"
if [ -e ~/.zsh/zsh-notify ]; then
  source ~/.zsh/zsh-notify/notify.plugin.zsh
fi

export LANG=ja_JP.UTF-8
export LESSCHARSET=utf-8

#rbenv
export PATH="$HOME/.rbenv/bin:$PATH"
[ -e ~/.rbenv ] && eval "$(rbenv init -)"

#nvm
nvm() {
    unset -f nvm
    source "${NVM_DIR:-$HOME/.nvm}/nvm.sh"

    nvm "$@"
}

#gradle completion
[ -f ${HOME}/.gradle_tabcomp ] && source ${HOME}/.gradle_tabcomp

#path
[ `uname` = "Darwin" ] && export PATH="/usr/local/bin:$PATH"

if [ -e /usr/share/terminfo/x/xterm-256color ]; then
        export TERM='xterm-256color'
else
        export TERM='xterm-color'
fi
